# Enter your code here. Read input from STDIN. Print output to STDOUT
from itertools import combinations
a = input().strip()
list_a = a.split(" ")
s = list_a[0]
k = int(list_a[1])
li_sorted = []
for i in range(len(s)):
  li_sorted.append(s[i])
li_sorted.sort()
s = "".join(li_sorted)
out = []
li = []
for i in range(k+1):
    li = list(combinations(s,i))
    li.sort()
    for j in range(len(li)):
        out.append(''.join(li[j]))       
for i in range(len(out)):
    if out[i] != "":
        print(out[i])       
